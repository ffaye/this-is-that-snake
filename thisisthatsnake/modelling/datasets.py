import os
import torch
from torchvision import transforms as T
from PIL import Image


class SnakeDataset(torch.utils.data.Dataset):
    """Torch Dataset for snake classification."""

    def __init__(
        self,
        label_map_df,
        class_map,
        image_dir="images",
        transforms=None,
    ):
        """
        Initialize the SnakeDataset.

        Args:
            label_map_df (pandas.DataFrame): Assumes that the label column is
                named "label" and that the image path column is named "id".
            class_map (dict): Mapping class string to int.
            image_dir (str): Directory with input images.
            transforms: torchvision.transform, or a list of them, operating on
                tensors.
        """
        self.label_map_df = label_map_df
        self.class_map = class_map
        self.filename_col = "id"
        self.label_col = "label"

        self.image_dir = image_dir
        if not os.path.isdir(self.image_dir):
            raise IOError(f"Image directory '{self.image_dir}' does not exist.")

        if isinstance(transforms, list):
            transforms = T.Compose(transforms)
        self.transforms = transforms

    def __len__(self):
        return len(self.label_map_df)

    def __getitem__(self, idx):
        image_path = os.path.join(
            self.image_dir, self.label_map_df.loc[idx, self.filename_col]
        )
        image_as_pil = Image.open(image_path)
        image = T.ToTensor()(image_as_pil)
        if self.transforms is not None:
            image = self.transforms(image)

        label = self.class_map[self.label_map_df.loc[idx, self.label_col]]

        return image, label
