import torch
from tqdm import tqdm
from time import time
from sklearn.metrics import f1_score


def freeze_layers(model, unfreeze_names=None, ignore_bn=True):
    """
    Freezes all layers in the passed model, and then unfreezes selected layers.

    Args:
        model: Torch model to freeze.
        unfreeze_names (list): List of layer or module names to unfreeze after
            all other layers have been frozen. Names can be obtained with
            model.named_modules(). Example: ["backbone.layer4.0.conv1"] will
            unfreeze only that specific layer, while ["backbone.layer4"] will
            unfreeze everything in that submodule.
        ignore_bn (bool): Whether BatchNorm layers should all be left unfrozen.
    """

    def _set_requires_grad(layer, set_to=False):
        for param in layer.parameters():
            param.requires_grad = set_to

    # Freeze everything
    for name, layer in model.named_modules():
        _set_requires_grad(layer, False)

    # Unfreeze anything batch norm related
    if ignore_bn:
        for name, layer in model.named_modules():
            if isinstance(layer, torch.nn.modules.batchnorm.BatchNorm2d):
                _set_requires_grad(layer, True)

    # Unfreeze anything in the ignore list
    if unfreeze_names is not None:
        for name, layer in model.named_modules():
            if name in unfreeze_names:
                _set_requires_grad(layer, True)


def iterate_through_dataloader(
    dataloader,
    model,
    optimizer,
    criterion,
    epoch,
    num_epochs,
    metric_names,
    train_mode=True,
    scheduler=None,
    device="cpu",
):
    """
    Iterate once through a dataloader.

    Args:
        dataloader (torch.utils.data.DataLoader): Dataloader instance to iterate
            through.
        model (torch.nn.Module): Model instance to predict or train with.
        optimizer (torch.optim): Optimizer instance to update the model with.
        criterion (torch.nn._Loss): Torch loss function to use for training.
        epoch (int): The current epoch.
        num_epochs (int): The total number of expected epochs.
        metric_names (list): Names of metrics that should be collected for each
            batch.
        train_mode (bool): Whether to use training model (model.train() with
            grads) or inference (model.eval() and no grads).
        scheduler (torch.optim.lr_scheduler): Learning rate scheduler instance.
        device (torch.Device): Device to compute on.

    Returns:
        dict: Losses and metrics averaged over the iteration of the dataloader.
    """

    def _process_batch(img_batch, label_batch):
        metrics = {name: 0.0 for name in metric_names}
        img_batch = img_batch.to(device)
        label_batch = label_batch.to(device)

        if train_mode:
            optimizer.zero_grad()

        logits = model(img_batch)

        # Losses
        loss = criterion(logits, label_batch)

        if train_mode:
            loss.backward()
            optimizer.step()

        # Metrics
        metrics["loss"] = loss.item()
        output_class = torch.argmax(logits, dim=1)
        metrics["acc"] = (output_class == label_batch).sum().item() / label_batch.size(0)
        # metrics["f1"] = f1_score_from_tensors(once_hot_output_class, label_batch)

        return logits, metrics

    running_metrics = {name: 0.0 for name in metric_names}
    model.train() if train_mode else model.eval()
    iterator = (
        tqdm(dataloader, desc=f"Epoch {epoch}/{num_epochs}")
        if train_mode
        else dataloader
    )

    with (torch.enable_grad() if train_mode else torch.no_grad()):
        for img_batch, label_batch in iterator:
            logits, batch_metrics = _process_batch(img_batch, label_batch)

            # Accumulate losses and metrics
            for name in metric_names:
                running_metrics[name] += batch_metrics[name]

    if train_mode and scheduler is not None:
        scheduler.step()

    # Get mean of losses and metrics
    for name in metric_names:
        running_metrics[name] /= len(dataloader)

    return running_metrics


def f1_score_from_tensors(output, target, average="macro"):
    """F1 score on tensors converted to numpy arrays."""

    target_numpy = target.cpu().detach().numpy()
    output_numpy = output.cpu().detach().numpy()

    return f1_score(target_numpy, output_numpy, average=average)


def print_history_result(history, start_time=None, prec=3):
    """Print training history dict on epoch end."""

    report_string = f"Epoch {history['epoch'][-1]}: "
    report_string += " -- ".join(
        f"{report}: {history[report][-1]:.{prec}f}"
        for report in history
        if report.lower() != "epoch"
    )
    if start_time is not None:
        end_time = time() - start_time
        report_string += f" -- completed in {end_time / 60:.1f} mins"
        
    print(report_string)
