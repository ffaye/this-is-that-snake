import os
import json
import pandas as pd
from collections import defaultdict
from time import time

import torch
from torch.utils.data import DataLoader
from torch.nn import CrossEntropyLoss
from torch.optim import AdamW
from torch.optim.lr_scheduler import CyclicLR
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms as T

from thisisthatsnake.modelling.datasets import SnakeDataset
from thisisthatsnake.modelling.models import ConvNetModel
from thisisthatsnake.modelling.utils import freeze_layers, iterate_through_dataloader, print_history_result
from thisisthatsnake.data import (
    CLASS_MAP_PATH,
    TRAIN_LABEL_MAP_PATH,
    TEST_LABEL_MAP_PATH,
    IMAGE_PREPROCESSED_DIR,
    EXPERIMENTS_DIR
)


# Hyperparameters
hps = {
    "timm_name": "efficientnet_b0",
    "use_pretrained": True,
    "freeze": True,
    "unfreeze_names": ["clf"],
    "num_units_in_clf": 128,
    "optimizer_kwargs": {},
    "learning_rate": 0.001,
    "lr_scheduler_kwargs": {
        "base_lr": 0.0001,
        "max_lr": 0.005,
        "cycle_momentum": False,
        "step_size_up": 1000
    },
    "class_weights": None,
    "num_epochs": 10,
    "batch_size": 6,
    "dataaug_color_jitter_kwargs": {
        "brightness": 0.35,
        "contrast": 0.2,
        "saturation": 0.2,
        "hue": 0.15,
    },
    "dataaug_random_affine_kwargs": {
        "degrees": 10,
        "scale": (0.85, 1.15),
        "translate": (0.15, 0.15),
    },
    "norm_mean": [0.485, 0.456, 0.406],
    "norm_std": [0.229, 0.224, 0.225],
    "num_workers": 4,
}

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print("Using device:", device)

# Create experiment directory
if not os.path.isdir(EXPERIMENTS_DIR):
    os.mkdir(EXPERIMENTS_DIR)
experiment_dir = os.path.join(EXPERIMENTS_DIR, str(int(time())))
os.mkdir(experiment_dir)

# Log hyperparameters
with open(os.path.join(experiment_dir, "hyperparameters.json"), "w") as fh:
    json.dump(hps, fh)

# Load label and class maps
train_df = pd.read_csv(TRAIN_LABEL_MAP_PATH)
test_df = pd.read_csv(TEST_LABEL_MAP_PATH)

with open(CLASS_MAP_PATH, "r") as fh:
    class_map = json.load(fh)

# Transforms
transforms_train = [
    T.ColorJitter(**hps["dataaug_color_jitter_kwargs"]),
    T.Normalize(mean=hps["norm_mean"], std=hps["norm_std"]),
    T.RandomHorizontalFlip(),
    T.RandomVerticalFlip(),
    T.RandomAffine(**hps["dataaug_random_affine_kwargs"]),
]
transforms_test = [T.Normalize(mean=hps["norm_mean"], std=hps["norm_std"])]

# Dataset and Dataloaders
train_dataset = SnakeDataset(
    train_df,
    class_map,
    image_dir=IMAGE_PREPROCESSED_DIR,
    transforms=transforms_train,
)
train_dataloader = DataLoader(
    train_dataset, batch_size=hps["batch_size"], shuffle=True, num_workers=hps["num_workers"]
)
test_dataset = SnakeDataset(
    test_df,
    class_map,
    image_dir=IMAGE_PREPROCESSED_DIR,
    transforms=transforms_test,
)
test_dataloader = DataLoader(
    test_dataset, batch_size=hps["batch_size"], shuffle=False, num_workers=hps["num_workers"]
)

# Model
model = ConvNetModel(
    num_classes=len(class_map),
    num_units_in_clf=hps["num_units_in_clf"],
    timm_name=hps["timm_name"]
)
model = model.to(device)
if hps["freeze"]:
    freeze_layers(model, unfreeze_names=hps["unfreeze_names"])

# Optimizer
optimizer = AdamW(
    filter(lambda p: p.requires_grad, model.parameters()),
    lr=hps["learning_rate"],
    **hps["optimizer_kwargs"],
)
scheduler = CyclicLR(optimizer, **hps["lr_scheduler_kwargs"])

# Loss
criterion = CrossEntropyLoss(weight=hps["class_weights"])

# Metrics
tb_dir = os.path.join(experiment_dir, "tensorboard")
tb_writer = SummaryWriter(tb_dir)
metric_names = ["loss", "acc"] # "f1"],
history = defaultdict(list)

# Train
iterate_through_dataloader_common_kwargs = {
    "model": model,
    "optimizer": optimizer,
    "criterion": criterion,
    "num_epochs": hps["num_epochs"],
    "metric_names": metric_names,
    "scheduler": scheduler,
    "device": device,
}
for epoch in range(1, hps["num_epochs"] + 1):
    start_time = time()
    running_metrics_train = iterate_through_dataloader(
        train_dataloader,
        epoch=epoch,
        train_mode=True,
        **iterate_through_dataloader_common_kwargs,
    )
    running_metrics_test = iterate_through_dataloader(
        test_dataloader,
        epoch=epoch,
        train_mode=False,
        **iterate_through_dataloader_common_kwargs,
    )

    # Write and report history
    history["epoch"].append(epoch)
    for name in metric_names:
        history[f"{name}_train"].append(running_metrics_train[name])
        history[f"{name}_val"].append(running_metrics_test[name])
        tb_writer.add_scalar(f"{name}/train", running_metrics_train[name], epoch)
        tb_writer.add_scalar(f"{name}/test", running_metrics_test[name], epoch)
    last_lr = scheduler.get_last_lr()[0]
    history["lr"].append(last_lr)
    tb_writer.add_scalar("lr", last_lr, epoch)
    with open(os.path.join(experiment_dir, "history.json"), "w") as fh:
        json.dump(history, fh)
    print_history_result(history, start_time)

tb_writer.close()

# Save model
torch.save(
    model.state_dict(),
    os.path.join(experiment_dir, "model_trained.pt")
)
