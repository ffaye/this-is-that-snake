import torch
import timm


class ConvNetModel(torch.nn.Module):
    """CNN with a timm backbone."""

    def __init__(
        self,
        num_classes,
        num_units_in_clf=None,
        timm_name="efficientnet_b0",
        pretrained=True,
    ):
        """
        Initialize the ConvNetModel.

        Args:
            num_classes (int): Number of classes the network should predict.
            num_units_in_clf (int): If not None, add a layer to the classification
                head with this many units in it.
            timm_name (str): Name used to fetch a backbone from the timm library.
            pretrained (bool): Whether to load weights pretrained on ImageNet1k.
        """
        super(ConvNetModel, self).__init__()
        self.backbone = timm.create_model(
            timm_name,
            pretrained=pretrained,
            features_only=True,
        )
        if num_units_in_clf:
            self.clf = torch.nn.Sequential(
                torch.nn.Linear(self.backbone.feature_info.channels()[-1], num_units_in_clf),
                torch.nn.BatchNorm1d(num_units_in_clf),
                torch.nn.ReLU(),
                torch.nn.Linear(num_units_in_clf, num_classes),
            )
        else:
            self.clf = torch.nn.Linear(self.backbone.feature_info.channels()[-1], num_classes)

    def forward(self, x):
        features = self.backbone(x)[-1]
        return self.clf(features.mean(dim=(-1, -2)))  # Global avg. pooling
