import argparse
import os
import torchvision.transforms as T
from PIL import Image
from glob import glob
from tqdm import tqdm

from thisisthatsnake.data import IMAGE_RAW_DIR, IMAGE_PREPROCESSED_DIR, METADATA_DIR


def transform_and_save_image(image_path, transforms, save_path, format="PNG"):
    """
    Load, transform and save an image.

    Args:
        image_path (str): Image to load.
        transforms (torchvision.transforms): Image transforms to be applied.
        save_path (type): Path to save the image to.
        format (str): Which format to save the image as.
    """

    with Image.open(image_path) as image:
        image = transforms(image.convert("RGB"))
    image_name = os.path.basename(image_path)
    image.save(save_path, format=format)


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--image_size",
        help="Height and width to resize images to. Default is 260.",
        type=int,
        default=260
    )
    parser.add_argument(
        "--image_save_format",
        help="Which format to save the transformed images in.",
        type=str,
        default="PNG"
    )
    args = parser.parse_args()

    if not os.path.isdir(IMAGE_PREPROCESSED_DIR):
        os.makedirs(IMAGE_PREPROCESSED_DIR)

    transforms = T.Compose([
        T.ToTensor(),
        T.Resize((args.image_size, args.image_size)),
        T.ToPILImage(),
    ])

    for image_path in tqdm(glob(os.path.join(IMAGE_RAW_DIR, "*"))):
        image_name = os.path.basename(image_path)
        save_path = os.path.join(IMAGE_PREPROCESSED_DIR, image_name)
        if os.path.isfile(save_path):
            tqdm.write(f"Image {image_name} has already been transformed")
        else:
            transform_and_save_image(
                image_path,
                transforms,
                save_path,
                format=args.image_save_format.upper()
            )
