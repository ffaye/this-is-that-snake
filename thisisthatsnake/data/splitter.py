import argparse
import pandas as pd
from sklearn.model_selection import train_test_split

from thisisthatsnake.data import LABEL_MAP_PATH, TRAIN_LABEL_MAP_PATH, TEST_LABEL_MAP_PATH


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--test_fraction",
        help="Fraction of data dedicated to the test set. Default is 0.1",
        type=float,
        default=0.2
    )
    args = parser.parse_args()

    # Load
    label_map_df = pd.read_csv(LABEL_MAP_PATH)[["id", "label"]]

    # Split
    train_label_map_df, test_label_map_df = train_test_split(
        label_map_df,
        test_size=args.test_fraction,
        stratify=label_map_df["label"]
    )

    # Save
    train_label_map_df.to_csv(TRAIN_LABEL_MAP_PATH, index=False)
    test_label_map_df.to_csv(TEST_LABEL_MAP_PATH, index=False)
