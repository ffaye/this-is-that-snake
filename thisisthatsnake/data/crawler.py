import argparse
import os
import re
import requests
import json
import shutil
import pickle
import hashlib
import praw
from glob import glob
from datetime import datetime
from psaw import PushshiftAPI
from tqdm import tqdm

from thisisthatsnake.data import IMAGE_RAW_DIR, METADATA_DIR, CREDENTIALS_PATH


_removed_image_hash = "35a0932c61e09a8c1cad9eec75b67a03602056463ed210310d2a09cf0b002ed5"


def get_api():
    """
    Return an authenticated PSAW API instance.

    Returns:
        psaw.PushshiftAPI: Authenticated API instance.
    """

    with open(CREDENTIALS_PATH, "r") as fh:
        auth_info = json.load(fh)

    reddit = praw.Reddit(
        client_id=auth_info["client_id"],
        client_secret=auth_info["client_secret"],
        user_agent=auth_info["user_agent"],
    )

    return PushshiftAPI(r=reddit)


def download_image(image_url, image_save_path, ignore_hash=_removed_image_hash):
    """
    Download an image from the specified URL.

    Args:
        image_url (str): URL of the image that should be downloaded.
        image_save_path (str): Local path that the image shold be saved to.
        ignore_hash (str): If the downloaded image has this SHA256 hash, ignore
            it and return False. This is particularly useful for ignoring empty
            or placeholder images.

    Returns:
        bool: Whether the image was successfully downloaded.
    """
    response = requests.get(image_url, stream=True).raw
    if hashlib.sha256(response.read()).hexdigest() == ignore_hash:
        status = False
    else:
        with open(image_save_path, "wb") as fh:
            shutil.copyfileobj(response, fh)
        status = True

    return status


def extract_species_from_comment(comment_body):
    """
    Extract a species name from a comment, according to r/whatsthissnake formatting.

    Args:
        comment_body (str): Body of text that a species name should be attempted
            to be extracted from.

    Returns:
        str: If there is a single species match, it is returned in a cleaned format,
            where whitespaces have been replaced with underscores. Else return None.
    """
    species_pattern = re.compile("\*(.*?)\*")
    species_matches = species_pattern.findall(comment_body)
    if len(species_matches) == 1:
        species_match = species_matches[0]

        # Remove any strikethroughs
        strikethrough_pattern = re.compile("(~~)(.*?)(~~)")
        strikethrough_matches = strikethrough_pattern.findall(species_match)
        if strikethrough_matches:
            for strikethrough_match in strikethrough_matches:
                species_match = species_match.replace("".join(strikethrough_match), "")

        # Remove non-letters
        species_match = "".join(c for c in species_match if c.isalpha() or c == " ")

        # Replace whitespaces of arbitrary length with a single underscore
        species_match = "_".join(species_match.split())

        return species_match.lower()


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--min_score",
        help="Minimum allowed Reddit post score. Default is 0.",
        type=int,
        default=0
    )
    parser.add_argument(
        "--after_date",
        help="Only collect data after this date, in YYYY-MM-DD format. "
        "Default is '2018-01-01'.",
        type=str,
        default="2018-01-01"
    )
    parser.add_argument(
        "--before_date",
        help="Only collect data before this date, in YYYY-MM-DD format. "
        "Default is today.",
        type=str,
        default=None
    )
    args = parser.parse_args()

    after_date = datetime.strptime(args.after_date, "%Y-%m-%d")
    if args.before_date is None:
        before_date = datetime.now()
    else:
        before_date = datetime.strptime(args.before_date, "%Y-%m-%d")

    if not os.path.isdir(IMAGE_RAW_DIR):
        os.makedirs(IMAGE_RAW_DIR)
    if not os.path.isdir(METADATA_DIR):
        os.makedirs(METADATA_DIR)

    image_dir_file_list = [os.path.basename(file_path) for file_path in glob(os.path.join(IMAGE_RAW_DIR, "*"))]
    metadata_dir_file_list = [os.path.basename(file_path) for file_path in glob(os.path.join(METADATA_DIR, "*"))]

    api = get_api()
    submissions = api.search_submissions(
        subreddit="whatsthissnake",
        score=f">{args.min_score}",
        after=int(after_date.timestamp()),
        before=int(before_date.timestamp()),
    )

    for submission in tqdm(submissions):
        try:
            if submission.num_comments > 0 and submission.url.endswith((".jpg", ".jpeg", ".png")):
                for comment in submission.comments:
                    author_flair = comment.author_flair_text
                    if author_flair and "reliable responder" in author_flair.lower():
                        species = extract_species_from_comment(comment.body)
                        if species:
                            # Prepare save paths
                            url_save_string = submission.url.replace("https://", "").replace("http://", "").replace("/", "#")
                            id_string = f"{submission.id}--{comment.id}--{url_save_string}--{species}"
                            image_save_path = os.path.join(IMAGE_RAW_DIR, id_string)
                            metadata_save_path = os.path.join(METADATA_DIR, id_string)

                            if os.path.isfile(image_save_path) and os.path.isfile(metadata_save_path):
                                tqdm.write(f"Datapoint {id_string} already exists")
                                continue
                            else:
                                # Save image, or skip it if it has been removed
                                download_status = download_image(submission.url, image_save_path)
                                if not download_status:
                                    continue

                                # Save metadata
                                metadata = {
                                    "post": submission,
                                    "first_reliable_comment": comment,
                                    "species": species,
                                }
                                with open(metadata_save_path, "wb") as fh:
                                    pickle.dump(metadata, fh, protocol=pickle.HIGHEST_PROTOCOL)

                                tqdm.write(f"Saved datapoint {id_string}")

                                # Stop searching for reliable responder comments once one has been found
                                break

        except requests.exceptions.ConnectionError:
            continue
