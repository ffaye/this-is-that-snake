import os


DATA_DIR = os.path.join("thisisthatsnake", "data")
CREDENTIALS_PATH = os.path.join(DATA_DIR, "credentials.json")
IO_DIR = os.path.join(DATA_DIR, "io")
IMAGE_DIR = os.path.join(IO_DIR, "images")
IMAGE_RAW_DIR = os.path.join(IMAGE_DIR, "raw")
IMAGE_PREPROCESSED_DIR = os.path.join(IMAGE_DIR, "preprocessed")
METADATA_DIR = os.path.join(IO_DIR, "metadata")
TAXONOMY_MAP_PATH = os.path.join(IO_DIR, "taxonomy_map.json")
LABEL_MAP_PATH = os.path.join(IO_DIR, "label_map.csv")
TRAIN_LABEL_MAP_PATH = os.path.join(IO_DIR, "train_label_map.csv")
TEST_LABEL_MAP_PATH = os.path.join(IO_DIR, "test_label_map.csv")
CLASS_MAP_PATH = os.path.join(IO_DIR, "class_map.json")
EXPERIMENTS_DIR = os.path.join(IO_DIR, "experiments")
