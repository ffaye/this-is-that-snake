import argparse
import os
import requests
import pickle
import json
import pandas as pd
from glob import glob
from tqdm import tqdm

from thisisthatsnake.data import METADATA_DIR, LABEL_MAP_PATH, TAXONOMY_MAP_PATH, CLASS_MAP_PATH


_species_synonyms = {
    "thamnophis_sauritus": "thamnophis_saurita",
    "agkistrodon_piscivorous": "agkistrodon_piscivorus"
}


def load_metadata_as_df():
    """
    Load saved metadata as format as a DataFrame.

    Returns:
        pandas.DataFrame: DataFrame containing label metadata, sorted by species.
    """

    label_metadata = {"id": [], "title": [], "comment": [], "species": []}
    for metadata_path in tqdm(glob(os.path.join(METADATA_DIR, "*"))):
        with open(metadata_path, "rb") as fh:
            metadata = pickle.load(fh)
        label_metadata["id"].append(metadata_path.rsplit("/", 1)[1])
        label_metadata["title"].append(metadata["post"].title)
        label_metadata["comment"].append(metadata["first_reliable_comment"].body)
        label_metadata["species"].append(metadata["species"])

    return pd.DataFrame.from_dict(label_metadata).sort_values(by="species").reset_index(drop=True)


def get_canonical_name_from_gbif(value):
    """
    Fuzzy match species name using the GBIF API.

    Args:
        value (str): Species name.

    Returns:
        str: If a canonical name is found in GBIF, return it, else return None.
    """

    species = value.replace("_", " ").strip().capitalize()
    classification = "kingdom=Animalia&phylum=Chordata&class=Reptilia&order=Squamata"
    parameters = "verbose=false&strict=false"
    response = requests.get(
        f"https://api.gbif.org/v1/species/match?&{parameters}&{classification}&name={species}"
    )
    if response.ok:
        resp = response.json()
        if (
            resp["kingdom"] == "Animalia" and
            resp["phylum"] == "Chordata" and
            resp["class"] == "Reptilia" and
            resp["order"] == "Squamata"
        ):
            canonical_name = resp["canonicalName"].lower().replace(" ", "_")
            if canonical_name != "squamata":  # Means that no specific name has been found
                return canonical_name


def get_binomial_name(value):
    """
    Split string into its binomial name.

    Args:
        value: Taxonomic name.

    Returns:
        str: If `value` is a string, return the binomial name contained in it.
    """
    if isinstance(value, str):
        names = value.split("_")
        if len(names) > 1:
            return "_".join(names[:2])


def get_taxonomy_map(species_value_counts, min_count=50):
    """
    Create taxonomy map incl. "other" category when less than min_count points.

    Args:
        species_value_counts (pandas.Series): Value counts of species.
        min_count (int): If a species" value count is not above this, assign it
            the "other" category.
    Returns:
        dict: Taxonomy mapping.
    """

    all_species = set(species_value_counts.index)
    abundant_species = set(species_value_counts[species_value_counts >= min_count].index)
    taxonomy_map = {label: label for label in abundant_species}
    other_species = all_species.difference(abundant_species)
    for species in other_species:
        taxonomy_map[species] = "other"

    return taxonomy_map


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--min_count_for_own_label",
        help="The count required for a species to get its own label. "
        "If below, it will be assigned the 'other' class. Default is 50.",
        type=int,
        default=50
    )
    args = parser.parse_args()

    # Structure as DataFrame
    print("Loading metadata files")
    species_df = load_metadata_as_df()

    # Add canonical name through fuzzy GBIF search
    print("Looking up canonical names through GBIF API")
    species_df["canonical_name"] = species_df["species"].apply(get_canonical_name_from_gbif)

    # Assert binomial form
    species_df["canonical_binomial_name"] = species_df["canonical_name"].apply(get_binomial_name)

    # Standardize ambiguous naming conventions
    species_df["canonical_binomial_name"] = species_df["canonical_binomial_name"].apply(
        lambda name: _species_synonyms[name] if name in _species_synonyms else name
    )

    # Drop rows that could not be assigned a canonical binomial name
    species_df = species_df.dropna(subset=["canonical_binomial_name"])

    # Map canonical binomial names to labels
    species_counts = species_df["canonical_binomial_name"].value_counts()
    taxonomy_map = get_taxonomy_map(species_counts, min_count=args.min_count_for_own_label)
    species_df["label"] = species_df["canonical_binomial_name"].apply(lambda name: taxonomy_map[name])

    # Save
    with open(TAXONOMY_MAP_PATH, "w") as fh:
        json.dump(taxonomy_map, fh)

    classes = species_df.label.unique()
    class_map = {label: idx for idx, label in enumerate(sorted(classes))}
    with open(CLASS_MAP_PATH, "w") as fh:
        json.dump(class_map, fh)

    species_df.to_csv(LABEL_MAP_PATH, index=False)
