# This Is That Snake

Snake classification based on [r/whatsthissnake](https://old.reddit.com/r/whatsthissnake) data using PyTorch.

## Usage
Run the following commands from the repository root folder, after having installed the `requirements.txt` in a virtual environment, and having provided [Reddit API credentials](https://github.com/reddit-archive/reddit/wiki/OAuth2-Quick-Start-Example#first-steps) in the form of a JSON placed at `thisisthatsnake/data/credentials.json` structured like
```json
{
  "client_id": "<MY_CLIENT_ID>",
  "client_secret": "<MY_CLIENT_SECRET>",
  "user_agent": "<MY_USER_AGENT>"
}
```

### Collect data
```bash
python -m thisisthatsnake.data.crawler --min_score 0 --after_date "2018-01-01"
```
This scrapes [r/whatsthissnake](https://old.reddit.com/r/whatsthissnake) for images with high-quality identifications, and saves them to `thisisthatsnake/data/io/images/raw` and their metadata to `thisisthatsnake/data/io/metadata`.

### Prepare data
```bash
python -m thisisthatsnake.data.prepare_labels --min_count_for_own_label 50
python -m thisisthatsnake.data.prepare_images --image_size 260 --image_save_format "PNG"
python -m thisisthatsnake.data.splitter --test_fraction 0.1
```
The first line extracts identifications from comments of Reddit users with a *Reliable Responder* flair, and standardizes these identifications into a label map, creating `thisisthatsnake/data/io/label_map.csv` and `thisisthatsnake/data/io/taxonomy_map.json`.
The second line resizes the images and saves them to `thisisthatsnake/data/io/images/preprocessed`.
The third line splits the data into `thisisthatsnake/data/io/train_label_map.csv` and `thisisthatsnake/data/io/test_label_map.csv`.

### Train model
```bash
python -m thisisthatsnake.modelling.train
```
This trains a CNN and saves its hyperparameters, training history and tensorboard log file to a created timestamped folder in `thisisthatsnake/data/io`.
The default parameters, which can be set in the `thisisthatsnake/modelling/train.py` script, has not been chosen on the basis of any hyperparameter tuning, so there should be ample room for improvement.
